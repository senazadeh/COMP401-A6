package a6;

/**
 * 	The BeltFullException class is used by implementations of Belt to signal when an attempt 
 *  is made to place a plate on the belt when the belt is full
 */

public class BeltFullException extends Exception {
	private Belt belt = null;
	
	// assigns Belt belt to this.belt
	public BeltFullException(Belt belt) {
		this.belt = belt;
	}
	
	// returns this belt
	public Belt getBelt() {
		return this.belt;
	}
}
