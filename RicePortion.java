package a4;

public class RicePortion implements IngredientPortion{
	private static Ingredient Rice = new Rice(); 
	private double amount = 0;
	
	public RicePortion (double amount) {
		if (amount < 0) {
			throw new RuntimeException("Amount is less than 0"); }
	
	this.amount = amount; 
	}

	public Ingredient getIngredient() {
		return Rice;
	}

	public String getName() {
		return Rice.getName();
	}

	public double getAmount() {
		return amount;
	}

	public double getCalories() {
		return Rice.getCaloriesPerOunce() * amount;
	}

	public double getCost() {
		return Rice.getPricePerOunce() * amount;
	}

	public boolean getIsVegetarian() {
		return Rice.getIsVegetarian();
	}

	public boolean getIsRice() {
		return Rice.getIsRice();
	}

	public boolean getIsShellfish() {
		return Rice.getIsShellfish();
	}

	public IngredientPortion combine(IngredientPortion other) {
		if (other == null) {
			return this; 
		} else if (!this.getName().equals(other.getName())) {
			throw new RuntimeException("Not the same ingregient"); 
		} else {
			RicePortion combinedRice = new RicePortion(this.getAmount() + other.getAmount());
			return combinedRice;
		}
	}	
}
