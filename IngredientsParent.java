package a4;

public class IngredientsParent implements Ingredient{
	private String name = "";
	private double pricePerOz =	0.00;
	private int caloriesPerOz =	0;
	private boolean Vegetarian = false;
	private boolean Rice = false;
	private boolean Shellfish =	false;
	
	public IngredientsParent (String name, double pricePerOz, int caloriesPerOz, boolean Vegetarian, boolean Rice, boolean Shellfish) {
	
	this.name= name;
	this.pricePerOz = pricePerOz;
	this.caloriesPerOz = caloriesPerOz;
	this.Vegetarian = Vegetarian;
	this.Rice = Rice;
	this.Shellfish = Shellfish;a
	}
	
	public String getName() {
		return name;
	}
	
	public double getCaloriesPerDollar() {
		return caloriesPerOz / pricePerOz;
	}
	
	public int getCaloriesPerOunce() {
		return caloriesPerOz;
	}
	
	public double getPricePerOunce() {
		return pricePerOz;
	}
	
	public boolean equals(Ingredient other) {
		return (this.getName().equals(other.getName()) &&
				this.getCaloriesPerOunce() == other.getCaloriesPerOunce() &&
				this.getPricePerOunce() - other.getPricePerOunce() < 0.01 && 
				this.getPricePerOunce() - other.getPricePerOunce() > -0.01 &&
				this.getIsVegetarian() == other.getIsVegetarian() &&
				this.getIsRice() == other.getIsRice() &&
				this.getIsShellfish() == other.getIsShellfish()); 
	}
	
	public boolean getIsVegetarian() {
		return Vegetarian;
	}
	
	public boolean getIsRice() {
		return Rice;
	}
	
	public boolean getIsShellfish() {
		return Shellfish;
	}
}

