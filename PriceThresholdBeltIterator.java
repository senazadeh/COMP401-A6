package a6;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class PriceThresholdBeltIterator implements Iterator<Plate> {
	private Belt belt;
	private int start_position;
	private double max_price;
	
	public PriceThresholdBeltIterator(Belt belt, int start_position, double max_price) {
		this .belt = belt;
		this.start_position = start_position;
		this.max_price = max_price;
	}

	// returns true if there is another plate located on the belt or false if the belt is completely empty
	public boolean hasNext() {
		for (int i = 0; i < belt.getSize(); i++) {
			if (belt.getPlateAtPosition(start_position) != null) {
				if (belt.getPlateAtPosition(start_position).getPrice() <= max_price) {
					return true;
				}
			}
			this.start_position++;
		}
		return false;
	}

	// returns the next plate located on the belt
	public Plate next() {
		if (this.hasNext() == false) {
			throw new NoSuchElementException();
		}
		this.start_position++;
		return belt.getPlateAtPosition(start_position - 1);
	}
}

