package a6;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class BeltIterator implements Iterator<Plate> {
	private Belt belt;
	private int start_position;
	private boolean nextCalled = false;

	public BeltIterator(Belt belt, int start_position) {
		this.belt = belt;
		this.start_position = start_position;
	}

	// returns true if there is another plate located on the belt or false otherwise
	public boolean hasNext() {
		for (int i = 0; i < belt.getSize(); i++) {
			if (belt.getPlateAtPosition(i) != null) {
				return true;
			}
		}
		return false;
	}

	// returns the next plate located on the belt
	public Plate next() {
		this.nextCalled = true;
		if (this.hasNext() == false) {
			throw new NoSuchElementException();
		}
		for (int i = 0; i < belt.getSize(); i++) {
			if (belt.getPlateAtPosition(start_position) !=  null) {
				start_position++;
				return belt.getPlateAtPosition(start_position - 1);
			} else {
				start_position++;
			}
		}
		return null;
	}

	// removes the next plate located on the belt
	public void remove() {
		if (this.nextCalled == false) {
			throw new IllegalStateException();
		}
		this.nextCalled = false;
		belt.removePlateAtPosition(start_position - 1);
	}
}
