package a6;

import java.util.Iterator;
import java.util.NoSuchElementException;

import a6.Plate.Color;

public class BeltImpl implements Belt {

	private int size = 0;
	private Plate[] sushiBelt = null;
	
	public BeltImpl(int belt_size) {
		if (belt_size < 0) {
			throw new IllegalArgumentException("belt size too small");
		}
		this.size = belt_size;
		this.sushiBelt = new Plate[belt_size];
	}

	// returns the size of this belt
	public int getSize() {
		return this.size;
	}

	// returns the plate at the given belt position
	public Plate getPlateAtPosition(int position) {
		int normalized_position = ((position % size) + size) % size;
		if (this.sushiBelt[normalized_position] == null) {
			return null;
		} else {
			return this.sushiBelt[normalized_position];
		}
	}

	// sets the Plate plate at the given belt position
	public void setPlateAtPosition(Plate plate, int position) throws BeltPlateException {
		int normalized_position = ((position % size) + size) % size;
		if (plate == null) {
			throw new IllegalArgumentException("plate is null"); 
		} else if (this.sushiBelt[normalized_position] != null) {
			throw new BeltPlateException(position, plate, this);
		} else {
			this.sushiBelt[normalized_position] = plate;
		}
	}

	// clears the plate at the given belt position
	public void clearPlateAtPosition(int position) {
		int normalized_position = ((position % size) + size) % size;
		this.sushiBelt[normalized_position] = null;
	}

	// returns the plate at the given position and then clears the plate at the given position
	public Plate removePlateAtPosition(int position) {
		int normalized_position = ((position % size) + size) % size;
		if (this.sushiBelt[normalized_position] == null) {
			throw new NoSuchElementException("no plate at postion");
		} else {
			Plate plateAtPosition = getPlateAtPosition(position);
			clearPlateAtPosition(position);
			return plateAtPosition;
		}
	}

	// returns the next empty postion on the belt from the given position and places the Plate plate at that location
	public int setPlateNearestToPosition(Plate plate, int position) throws BeltFullException {
		for (int i = 0; i < getSize(); i++) {
			int incrementedPosition = position + i;
			int normalized_position = ((incrementedPosition % size) + size) % size;
			if (sushiBelt[normalized_position] == null) {
				sushiBelt[normalized_position] = plate;;
				return normalized_position;
			}
		}
		throw new BeltFullException(null);
	}
	
	// returns a plate iterator that iterates through all plates on the belt
	public Iterator<Plate> iterator() {
		Iterator<Plate> iter = new BeltIterator(this, 0);
		return iter;
	}

	// returns a plate iterator that iterates through all plates starting from the given position
	public Iterator<Plate> iteratorFromPosition(int position) {
		int normalized_position = ((position % size) + size) % size;
		Iterator<Plate> iter = new BeltIterator(this, normalized_position);
		return iter;
	}
	
	// moves every plate on this belt to the next position
	public void rotate() {
		Plate[] rotated = new Plate[sushiBelt.length];
		for (int i = 0; i < sushiBelt.length; i++) {
			int position = i - 1;
			int normalized_position = ((position % size) + size) % size;
			rotated[i] = this.sushiBelt[normalized_position];
		}
		this.sushiBelt = rotated;
	}

	// returns a plate iterator that iterates through all plates thats cost is less or equal to the given max price on this belt
	public Iterator<Plate> iterator(double max_price) {
		return iteratorFromPosition(0, max_price);
	}
	
	// returns a plate iterator that iterates through all plates that are of Color color on this belt
	public Iterator<Plate> iterator(Color color) {
		return iteratorFromPosition(0, color);
	}

	// returns a plate iterator that iterates through all plates thats cost is less or equal to the given max price on this belt
	// starting at the given positon 
	public Iterator<Plate> iteratorFromPosition(int position, double max_price) {
		Iterator<Plate> iter = new PriceThresholdBeltIterator(this, position, max_price);
		return iter;
	}

	// returns a plate iterator that iterates through all plates that are of Color color on this belt starting at the given positon
	public Iterator<Plate> iteratorFromPosition(int position, Color color) {
		Iterator<Plate> iter = new ColorFilteredBeltIterator(this, position, color);
		return iter;
	}
}




