package a4;

public class Eel extends IngredientsParent implements Ingredient{
	private static String name = "eel";
	private static double pricePerOz =	2.15;
	private static int caloriesPerOz =	82;
	private static boolean Vegetarian = false;
	private static boolean Rice = false;
	private static boolean Shellfish =	false;	
	
	public Eel() { 
		super(name, pricePerOz, caloriesPerOz, Vegetarian, Rice, Shellfish);
	}
	
	public String getName() {
		return name;
	}
	
	public double getCaloriesPerDollar() {
		return caloriesPerOz / pricePerOz;
	}
	
	public int getCaloriesPerOunce() {
		return caloriesPerOz;
	}
	
	public double getPricePerOunce() {
		return pricePerOz;
	}
	
	public boolean equals(Ingredient other) {
		return (this.getName().equals(other.getName()) &&
				this.getCaloriesPerOunce() == other.getCaloriesPerOunce() &&
				this.getPricePerOunce() - other.getPricePerOunce() < 0.01 && 
				this.getPricePerOunce() - other.getPricePerOunce() > -0.01 &&
				this.getIsVegetarian() == other.getIsVegetarian() &&
				this.getIsRice() == other.getIsRice() &&
				this.getIsShellfish() == other.getIsShellfish()); 
	}
	
	public boolean getIsVegetarian() {
		return Vegetarian;
	}
	
	public boolean getIsRice() {
		return Rice;
	}
	
	public boolean getIsShellfish() {
		return Shellfish;
	}
}

